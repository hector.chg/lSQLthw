# Apuntes SQL

**Create**	Putting data into tables (or create tables).
**Read**	Query data out of a table.
**Update**	Change data already in a table.
**Delete**	Remove data from the table.

- A database is a whole spreadsheet file.
- A table is a tab/sheet in the spreadsheet, with each one being given a name.
- A column is a column in both.
- A row is a row in both.
- SQL then gives you a language for doing CRUD operations on these to produce new tables or alter existing ones.

The last item is significant, and not understanding this causes people a lot of headaches. SQL only knows tables, and every operation produces tables. It either ”produces” a table by modifying an existing one, or it returns a new temporary table as your data set.
